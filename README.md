## Description
A service to export mongodb to csv.

## Install the dependencies
1. Install and start mongodb container
```bash
docker run --name mongodb -d -p 27017:27017 mongo
```  
2. Create a Mongodb database named `testing`  
3. Install dependencies
```bash
npm install
```  

## Running the app

```bash
# development
nest start mongo-exporter

# watch mode
nest start mongo-exporter --watch

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```  

## Todo
- [ ] fields selection exporting