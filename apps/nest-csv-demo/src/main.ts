import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  await app.listen(app.get(ConfigService).get('nestCsvDemoPort'));
  logger.log(
    `Application is running on: ${await app.getUrl()} with ${process.env.NODE_ENV
    } environment!`,
    'Bootstrap',
  );
}
bootstrap();
