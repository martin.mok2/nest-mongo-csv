import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from 'config/configuration';
import { MongoCsvModule } from '@app/mongo-csv';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      envFilePath: '.env',
      isGlobal: true,
    }),
    MongoCsvModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
