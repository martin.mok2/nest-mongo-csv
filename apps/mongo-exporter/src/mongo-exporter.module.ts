import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from 'config/configuration';
import { MongoExporterController } from './mongo-exporter.controller';
import { MongoExporterService } from './mongo-exporter.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoCsvModule } from '@app/mongo-csv';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      envFilePath: '.env',
      isGlobal: true,
    }),
    MongoCsvModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('mongodbURI'),
        dbName: configService.get<string>('mongodbDbName'),
        // useNewUrlParser: true,
        // useCreateIndex: true,
        // useFindAndModify: false,
      }),
    }),
    // MongooseModule.forRoot(`${process.env.MONGODB_URI}/${process.env.MONGODB_DB_NAME}`)
  ],
  controllers: [MongoExporterController],
  providers: [MongoExporterService],
})
export class MongoExporterModule {}
