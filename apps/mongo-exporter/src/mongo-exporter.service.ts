import { MongoCsvService } from '@app/mongo-csv';
import { Injectable, Logger, StreamableFile } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { createReadStream, createWriteStream } from 'fs';
import { join } from 'path';
import { format } from 'fast-csv';

@Injectable()
export class MongoExporterService {
  private readonly logger: Logger
  constructor(
    private readonly mongoCsvService: MongoCsvService,
    @InjectConnection() private readonly connection: Connection,

  ) { this.logger = new Logger() }

  async exportCSV(
    modelName: string,
    filePath?: string,
    fileName?: string,
  ): Promise<StreamableFile> {
    try {
      console.log('start exportCSV in service');

      await this.mongoCsvService.exportCSV(modelName, filePath, fileName);
      // await this._export(modelName, filePath, fileName);

      console.log('end exportCSV in service');
      const file = createReadStream(join(process.cwd(), '.prettierrc'));
      return new StreamableFile(file);
    } catch (error) {
      console.log(error);
      throw Error('export error');
    }
  }

  async _export(modelName: string, filePath?: string, fileName?: string) {
    try {
      console.time(`export ${fileName} time`);

      const csvStream = format({ headers: true });
      const fullFilePath = join(filePath, fileName);
      this.logger.log('fullFilePath: ', fullFilePath);

      const writeStream = createWriteStream(fullFilePath, { flags: 'w' });
      csvStream.pipe(writeStream);
      csvStream.on('end', () => console.log('done'));
      csvStream.on('error', (err) => console.error(err));

      this.logger.log(`Start export ${fileName}`);
      const model = this.connection.collection(modelName);
      this.logger.log('databaseName: ', this.connection.db.databaseName);

      this.logger.log(`model.name: `, model.name);
      const cursor = model.find();
      for await (const doc of cursor) {
        this.logger.log(doc);

        csvStream.write(doc);
      }
      this.logger.log(`Finish export ${fileName}`);

      console.timeEnd(`export ${fileName} time`);

      csvStream.end();
      writeStream.end();
    } catch (e) {
      console.error(e);
    }
  }
}
