import { Body, Controller, Get, Post, StreamableFile } from '@nestjs/common';
import { ExportCSVReqDto } from './ExportCSVReqDto';
import { MongoExporterService } from './mongo-exporter.service';

@Controller()
export class MongoExporterController {
  constructor(private readonly mongoExporterService: MongoExporterService) {}

  @Get('get-hello')
  getHello() {
    return "hello";
  }

  @Post('export-csv')
  exportCSV(@Body() exportCSVReqDto: ExportCSVReqDto) {
    console.log("exportCSVReqDto: ", exportCSVReqDto);

    const { modelName, filePath, fileName } = exportCSVReqDto;
    return this.mongoExporterService.exportCSV(
      modelName,
      filePath,
      fileName,
    );
  }
}
