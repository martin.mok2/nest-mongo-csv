import { Test, TestingModule } from '@nestjs/testing';
import { MongoExporterController } from './mongo-exporter.controller';
import { MongoExporterService } from './mongo-exporter.service';

describe('MongoExporterController', () => {
  let mongoExporterController: MongoExporterController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MongoExporterController],
      providers: [MongoExporterService],
    }).compile();

    mongoExporterController = app.get<MongoExporterController>(MongoExporterController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(mongoExporterController.getHello()).toBe('Hello World!');
    });
  });
});
