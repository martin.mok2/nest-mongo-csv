import { NestFactory } from '@nestjs/core';
import { MongoExporterModule } from './mongo-exporter.module';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(MongoExporterModule);
  await app.listen(app.get(ConfigService).get('mongoExporterPort'));
  logger.log(
    `Application is running on: ${await app.getUrl()} with ${
      process.env.NODE_ENV
    } environment!`,
    'Bootstrap',
  );
}
bootstrap();
