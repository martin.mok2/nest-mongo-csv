import { IsString } from 'class-validator';

export class ExportCSVReqDto {
  @IsString()
  readonly modelName: string;
  @IsString()
  readonly filePath: string;
  @IsString()
  readonly fileName: string;
}
