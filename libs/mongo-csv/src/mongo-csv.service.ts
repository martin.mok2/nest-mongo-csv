import { COLLECTION_NAME_TESTING } from './models/testing.schema';
import { Injectable, Logger } from '@nestjs/common';
import { format } from 'fast-csv';
import { createWriteStream } from 'fs';
import { TestingRepository } from './models/testing.repository';
import { join } from 'path';
import { Cursor, Connection } from "mongoose";
import { InjectConnection } from '@nestjs/mongoose';

const modelNameList = [COLLECTION_NAME_TESTING];
@Injectable()
export class MongoCsvService {
  private count = 1;
  constructor(
    private readonly testingRepository: TestingRepository,
    @InjectConnection() private readonly connection: Connection,
  ) {}

  async exportCSV(modelName: string, filePath?: string, fileName?: string) {
    try {
      console.log('in exportCSV');
      console.log('modelNameList: ', modelNameList);
      console.log("modelName: ", modelName);

      if (!modelNameList.includes(modelName)) return;
      fileName = (this.count++) +'_'+ fileName;
      console.time(`export ${fileName} time`);

      const csvStream = format({ headers: true });
      const fullFilePath = join(filePath, fileName);
      console.log('fullFilePath: ', fullFilePath);

      const writeStream = createWriteStream(fullFilePath, { flags: 'w' });
      csvStream.pipe(writeStream);
      csvStream.on('end', () => console.log('done'));
      csvStream.on('error', (err) => console.error(err));

      console.log(`Start export ${fileName}`);
      const model = this.connection.collection(modelName);
      console.log('databaseName: ', this.connection.db.databaseName);

      console.log(`model.name: `, model.name);
      const cursor = model.find();
      for await (const doc of cursor) {
        console.log(doc);

        csvStream.write(doc);
      }
      console.log(`Finish export ${fileName}`);

      console.timeEnd(`export ${fileName} time`);

      csvStream.end();
      writeStream.end();
      console.log('end exportCSV');
    } catch (e) {
      console.error(e);
    }
  }
}
