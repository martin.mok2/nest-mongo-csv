import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

export type TestingDocument = Testing & Document;
export const COLLECTION_NAME_TESTING = 'testing';

@Schema({
  validateBeforeSave: true,
  versionKey: false,
  collection: COLLECTION_NAME_TESTING
})
export class Testing {
  _id: ObjectId;

  @Prop({
    type: String,
    default: null,
    index: true,
  })
  email: string;

  @Prop({
    type: Number,
    default: 0,
  })
  balance: number;

  @Prop({
    type: Date,
    default: Date.now,
    required: true,
  })
  create_dt: Date;

  @Prop({
    type: Date,
    default: Date.now,
    required: true,
  })
  update_dt: Date;
}

export const TestingSchema = SchemaFactory.createForClass(Testing);
