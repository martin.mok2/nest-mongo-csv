import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Testing, TestingDocument } from './testing.schema';

@Injectable()
export class TestingRepository {
  constructor(
    @InjectModel(Testing.name)
    private readonly testingModel: Model<TestingDocument>,
  ) {}

  async getCursor() {
    // const filterQuery = [{
    //   $match: {
    //     // your pipeline here
    //   }
    // }, {
    //   $sort: {
    //     _id: -1
    //   }
    // }, {
    //   $limit: 50000
    // }];
    // return this.testingModel.find(filterQuery).cursor();
    return this.testingModel.find().cursor();
  }
}
