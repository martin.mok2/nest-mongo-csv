import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TestingRepository } from './models/testing.repository';
import { Testing, TestingSchema } from './models/testing.schema';
import { MongoCsvService } from './mongo-csv.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Testing.name, schema: TestingSchema }])
  ],
  providers: [MongoCsvService, TestingRepository],
  exports: [MongoCsvService, TestingRepository],
})
export class MongoCsvModule {}
