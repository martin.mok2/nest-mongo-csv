import { Test, TestingModule } from '@nestjs/testing';
import { MongoCsvService } from './mongo-csv.service';

describe('MongoCsvService', () => {
  let service: MongoCsvService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MongoCsvService],
    }).compile();

    service = module.get<MongoCsvService>(MongoCsvService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
