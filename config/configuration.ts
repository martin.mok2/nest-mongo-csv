export default () => ({
  mongodbURI: process.env.MONGODB_URI,
  mongodbDbName: process.env.MONGODB_DB_NAME,
  nestCsvDemoPort: process.env.NEST_CSV_DEMO_PORT || '',
  mongoExporterPort: process.env.MONGO_EXPORTER_PORT || '',
});
